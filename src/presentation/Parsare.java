package presentation;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.*;
import java.util.stream.Stream;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;


import dao.*;
import model.*;

public class Parsare {
	public ArrayList<String> parsare = new ArrayList<String>();
	public DAOCumparator client = new DAOCumparator();
	public DAOProdus produs = new DAOProdus();
	public DAOComanda comanda = new DAOComanda();
	public DAOProdusComandat produsComandat = new DAOProdusComandat();
	static int indexClient = 1;
	static int indexProdus = 1;
	static int indexComanda = 1;
	static int indexPDF1 = 1;
	static int indexPDF2 = 1;
	static int indexPDF3 = 1;
	static int indexPDF4 = 1;
	public static ArrayList<ProdusComandat> orderProdusComandat = new ArrayList<ProdusComandat>();

	public Parsare(String string) {
		String[] regex = string.split("[:,]");
		for (int i = 0; i < regex.length; i++) {
			if (regex[i].charAt(0) == ' ') {
				regex[i] = regex[i].substring(1);
			}
			System.out.println(regex[i] + " ");
			parsare.add(regex[i]);
		}
	}

	public void selectieComandaSQL() throws Exception {
		if (parsare.get(0).compareTo("Insert client") == 0 || parsare.get(0).compareTo("Insert Client") == 0)
			inserareClienti();
		if (parsare.get(0).compareTo("Delete client") == 0 || parsare.get(0).compareTo("Delete Client") == 0)
			stergereClienti();
		if (parsare.get(0).compareTo("Insert product") == 0 || parsare.get(0).compareTo("Insert Product") == 0)
			inserareProdus();
		if (parsare.get(0).compareTo("Delete product") == 0 || parsare.get(0).compareTo("Delete Product") == 0)
			stergereProdus();
		if (parsare.get(0).compareTo("Order") == 0)
			creareComanda();
		if (parsare.get(0).compareTo("Report client") == 0 || parsare.get(0).compareTo("Report Client") == 0)
			raportClienti();
		if (parsare.get(0).compareTo("Report order") == 0 || parsare.get(0).compareTo("Report Order") == 0)
			raportComenzi();
		if (parsare.get(0).compareTo("Report product") == 0 || parsare.get(0).compareTo("Report Product") == 0)
			raportProduse();
	}

	public void inserareClienti() throws Exception {
		client.insert(indexClient + ", '" + parsare.get(1) + "', '" + parsare.get(2) + "'");
		indexClient++;
	}

	public void stergereClienti() throws Exception {
		client.delete("numeClient", parsare.get(1));
	}

	public void inserareProdus() {
		String mux = "";
		Produs auxProdus = new Produs();

		try {
			auxProdus = produs.findByName(parsare.get(1), "numeProdus");
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		if (auxProdus != null) {
			int rez = auxProdus.getCantitate() + Integer.parseInt(parsare.get(2));
			System.out.println(rez);
			mux += rez;
			try {
				produs.update(mux, parsare.get(1));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		else {try {
			produs.insert(indexProdus + ", '" + parsare.get(1) + "', " + parsare.get(2) + ", " + parsare.get(3));
			indexProdus++;
		} catch (Exception e) {
			e.printStackTrace();
		}}
	}

	public void stergereProdus() throws Exception {
		produs.delete("numeProdus", parsare.get(1));
	}

	public void updateProdus(String str1, String str2) throws Exception {
		produs.update(str1, str2);
	}

	public void creareComanda() throws Exception {
		Cumparator auxClient = new Cumparator();
		auxClient = client.findByName(parsare.get(1), "numeClient");
		Produs auxProdus = new Produs();
		auxProdus = produs.findByName(parsare.get(2), "numeProdus");
		if (auxProdus.getCantitate() - Integer.parseInt(parsare.get(3)) >= 0) {
			int id = indexComanda; indexComanda++;
			comanda.insert(id + ", " + auxClient.getIdClient());
			produsComandat.insert(id + ", " + auxProdus.getIdProdus() + ", " + parsare.get(3) + ", "
					+ (Integer.parseInt(parsare.get(3)) * auxProdus.getPret()));
			orderProdusComandat.add(new ProdusComandat(id, auxProdus.getIdProdus(), Integer.parseInt(parsare.get(3)), (Integer.parseInt(parsare.get(3)) * auxProdus.getPret())));
			String crocodil = "";
			int auxCroco = (auxProdus.getCantitate() - Integer.parseInt(parsare.get(3)));
			crocodil += auxCroco;
			updateProdus(crocodil, auxProdus.getNumeProdus());
			generareBon(true, auxProdus.getNumeProdus(), Integer.parseInt(parsare.get(3)), auxCroco);
		}
		else {
			generareBon(false, "", 0, 0.0);
		}
	}
	
	public void generareBon(boolean disponibilitateProdus, String produs, int cantitate, double pret) throws FileNotFoundException, DocumentException {
		String file_name = "D:\\Rapoarte\\Generare_Bon_" + indexPDF4;
		indexPDF4++;
		Document document = new Document();
		PdfWriter.getInstance(document, new FileOutputStream(file_name));
		document.open();
		if(disponibilitateProdus) {
			Paragraph p = new Paragraph("" + produs + "      cantitate: " + cantitate + "     pret: "+ pret + "lei");
			document.add(p);
		}
		else {
			Paragraph p = new Paragraph("Stocul magazinului este sub cantitatea dorita");
			document.add(p);
		}
		document.close();
	}

	public void raportProduse() throws Exception {
		String file_name = "D:\\Rapoarte\\Raport_Produse_" + indexPDF1;
		indexPDF1++;
		Document document = new Document();
		PdfWriter.getInstance(document, new FileOutputStream(file_name));
		document.open();
		ArrayList<Produs> listaProduse = produs.findAll();
		for (Produs p : listaProduse) {
			PdfPTable table = new PdfPTable(4);
			String aux1 = "", aux2 = "", aux3 = "";
			aux1 += p.getIdProdus();
			aux2 += p.getCantitate();
			aux3 += p.getPret();
			Stream.of(aux1, p.getNumeProdus(), aux2, aux3).forEach(columnTitle -> {
				PdfPCell header = new PdfPCell();
				header.setBackgroundColor(BaseColor.LIGHT_GRAY);
				header.setBorderWidth(2);
				header.setPhrase(new Phrase(columnTitle));
				table.addCell(header);
			});

			document.add(table);
		}

		document.close();
	}

	public void raportClienti() throws Exception {
		String file_name = "D:\\Rapoarte\\Raport_Clienti_" + indexPDF2;
		indexPDF2++;
		Document document = new Document();
		PdfWriter.getInstance(document, new FileOutputStream(file_name));
		document.open();
		ArrayList<Cumparator> listaCumparatori = client.findAll();
		for (Cumparator p : listaCumparatori) {
			PdfPTable table = new PdfPTable(3);
			String aux1 = "";
			aux1 += p.getIdClient();
			Stream.of(aux1, p.getNumeClient(), p.getAdresa()).forEach(columnTitle -> {
				PdfPCell header = new PdfPCell();
				header.setBackgroundColor(BaseColor.LIGHT_GRAY);
				header.setBorderWidth(2);
				header.setPhrase(new Phrase(columnTitle));
				table.addCell(header);
			});

			document.add(table);
		}

		document.close();
	}

	public void raportComenzi() throws Exception {
		String file_name = "D:\\Rapoarte\\Raport_Comenzi_" + indexPDF3;
		indexPDF3++;
		Document document = new Document();
		PdfWriter.getInstance(document, new FileOutputStream(file_name));
		document.open();
		for (ProdusComandat p : orderProdusComandat) {
			PdfPTable table = new PdfPTable(4);
			String aux1 = "", aux2 = "", aux3 = "", aux4 = "";
			aux1 += p.getIdComanda(); aux2 += p.getIdProdus(); aux3 += p.getCantitate(); aux4 += p.getTotal();
			Stream.of(aux1, aux2, aux3, aux4).forEach(columnTitle -> {
				PdfPCell header = new PdfPCell();
				header.setBackgroundColor(BaseColor.LIGHT_GRAY);
				header.setBorderWidth(2);
				header.setPhrase(new Phrase(columnTitle));
				table.addCell(header);
			});

			document.add(table);
		}

		document.close();
	}

}
