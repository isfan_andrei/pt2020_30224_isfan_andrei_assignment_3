package start;

import java.io.*;
import java.util.*;
import presentation.Parsare;

public class StartClass {

	public static void main(String[] args) throws Exception {
		FileInputStream file = new FileInputStream(args[0]);
		InputStreamReader fchar = new InputStreamReader(file);
		BufferedReader buf = new BufferedReader(fchar);
		String str = buf.readLine();
		while(str != null) {
			Parsare parsare = new Parsare(str);
			parsare.selectieComandaSQL();
			str = buf.readLine();
		}
	}
}
