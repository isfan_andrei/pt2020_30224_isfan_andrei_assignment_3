package dao;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import connection.ConnectionFactory;
@SuppressWarnings("unchecked")

public class AbstractDAO<T> {
	protected static final Logger LOGGER = Logger.getLogger(AbstractDAO.class.getName());
	protected final Class<T> type;

	public AbstractDAO() {
		this.type = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
	}

	private String createSelectQuery(String field) {
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT");
		sb.append(" * ");
		sb.append("FROM ");
		sb.append(type.getSimpleName());
		sb.append(" WHERE " + field + " = ?");
		System.out.println(sb.toString());
		return sb.toString();
	}
	
	private String createSelectQuery() {
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT");
		sb.append(" * ");
		sb.append("FROM ");
		sb.append(type.getSimpleName());
		return sb.toString();
	}
	
	private String createDeleteQuery(String field, String nume) {
		StringBuilder sb = new StringBuilder();
		sb.append("delete");
		sb.append(" FROM ");
		sb.append(type.getSimpleName());
		sb.append(" WHERE " + field + " = '" + nume + "';");
		System.out.println(sb.toString());
		return sb.toString();
	}
	
	private String createInsertQuery(String comanda) {
		StringBuilder sb = new StringBuilder();
		sb.append("insert ");
		sb.append("into ");
		sb.append(type.getSimpleName());
		sb.append(" value (");
		sb.append(comanda);
		sb.append(");");
		System.out.println(sb.toString());
		return sb.toString();
	}
	
	private String createUpdateQuery(String ceSeModifica, String conditie) {
		StringBuilder sb = new StringBuilder();
		sb.append("update ");
		sb.append(type.getSimpleName());
		sb.append(" set cantitate = ");
		sb.append(ceSeModifica);
		sb.append(" where numeProdus = '");
		sb.append(conditie + "';");
		System.out.println(sb.toString());
		return sb.toString();
	}
	
	public void update(String str1,String str2) throws Exception
	{   
		Connection connection= null;
		String query=createUpdateQuery(str1,str2);
		try
		{
           connection = ConnectionFactory.getConnection();
			PreparedStatement st=connection.prepareStatement(query);
			st.executeUpdate();
		}
		catch(Exception e)
		{
			System.out.println("Eroare update");
		}
	}
	
	
	public void insert(String str) throws Exception
	{   
		Connection connection= null;
		String query=createInsertQuery(str);
		try
		{
           connection = ConnectionFactory.getConnection();
			PreparedStatement st = connection.prepareStatement(query);
			st.executeUpdate();
		}
		catch(Exception e)
		{
			System.out.println("Eroare insert");
		}
	}
	
	
	public void delete(String str, String str2) throws Exception
	{   
		Connection connection= null;
		String query=createDeleteQuery(str, str2);
		try
		{
            connection = ConnectionFactory.getConnection();
			PreparedStatement st=connection.prepareStatement(query);
			st.executeUpdate();
		}
		catch(Exception e)
		{
			System.out.println("Eroare delete");
		}
	}
	
	public ArrayList<T> findAll() throws Exception{
		ArrayList<T> lista = new ArrayList<T>();
		Connection connection=null;
		String query = createSelectQuery();
		try {
			connection = ConnectionFactory.getConnection();
			PreparedStatement statement = connection.prepareStatement(query);

			ResultSet resultSet = statement.executeQuery();
			
			while(resultSet.next()) {
				T instance = type.newInstance();
				for(Field field: type.getDeclaredFields()) {
					Object value = resultSet.getObject(field.getName());
					PropertyDescriptor pD = new PropertyDescriptor(field.getName(), type);
					Method method = pD.getWriteMethod();
					method.invoke(instance, value);
				}
				lista.add(instance);
			}
		}
		catch(SQLException e) {
			LOGGER.log(Level.WARNING,type.getName()+" DAO:findById "+e.getMessage());
			
		}catch(Exception e) {
			System.out.println("Eroaree");
		}
		finally {
			ConnectionFactory.close(connection);	
		}
		return lista;
	}
	
	public T findById(int id, String nume) throws SQLException{
		Connection connection=null;
		PreparedStatement statement=null;
		ResultSet resultSet=null;
		String query=createSelectQuery(nume);
		try {
			connection = ConnectionFactory.getConnection();
			statement=connection.prepareStatement(query);
			statement.setInt(1, id);
			resultSet=statement.executeQuery();
			
			return createObjects(resultSet).get(0);
		}
		catch(SQLException e) {
			LOGGER.log(Level.WARNING,type.getName()+" DAO:findById "+e.getMessage());
		}catch(Exception e) {}
		finally {
			ConnectionFactory.close(connection);
			ConnectionFactory.close(resultSet);
			ConnectionFactory.close(statement);
		}
		return null;
	}
	
	public T findByName(String nume, String nume2) throws Exception{
		Connection connection=null;
		PreparedStatement statement=null;
		ResultSet resultSet=null;
		String query=createSelectQuery(nume2);
		try {
			connection=ConnectionFactory.getConnection();
			statement=connection.prepareStatement(query);
			statement.setString(1, nume);
			resultSet=statement.executeQuery();
			
			return createObjects(resultSet).get(0);
		}
		catch(SQLException e) {
			LOGGER.log(Level.WARNING,type.getName()+" DAO:findByName "+e.getMessage());
		}catch(Exception e) {
			System.out.println("Prima inserare a acestui produs");
		}
		finally {
			ConnectionFactory.close(connection);
			ConnectionFactory.close(resultSet);
			ConnectionFactory.close(statement);
		}
		return null;
	}
	
	private List<T> createObjects(ResultSet resultSet) throws Exception
	{
		List<T> list=new ArrayList();
		try {
			while(resultSet.next())
			{
				T instance=type.newInstance();
				for(Field field: type.getDeclaredFields())
				{
					Object value = resultSet.getObject(field.getName());
					PropertyDescriptor propertyDescriptor = new PropertyDescriptor(field.getName(),type);
					Method method = propertyDescriptor.getWriteMethod();
					method.invoke(instance,value);
				}
				list.add(instance);
			}	
		}catch(InstantiationException e)
		{
			System.out.println("Eroare!");
		}	
		return list;
	}
}
