package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;

import connection.ConnectionFactory;
import model.Produs;

public class DAOProdus extends AbstractDAO<Produs>{

	private String creazaSelectPretQueryNume(String nume) {
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT idProdus FROM Produs WHERE numeProdus = ");
		sb.append(nume);
		return sb.toString();
	}
	
	public Double gasesteDupaPret(String nume) throws SQLException {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		String query = creazaSelectPretQueryNume(nume);
		try {
			connection = ConnectionFactory.getConnection();
			statement = connection.prepareStatement(query);
			statement.setString(2, nume);
			resultSet = statement.executeQuery();
			if(resultSet.next()) {
				return resultSet.getDouble(4);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "ProductDAO:findByName - get ID" + e.getMessage());
		} finally {
			ConnectionFactory.close(resultSet);
			ConnectionFactory.close(statement);
			ConnectionFactory.close(connection);
		}
		return null;
	}
	
}
