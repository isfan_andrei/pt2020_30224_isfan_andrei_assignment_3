package model;

public class ProdusComandat {
	private int idComanda;
	private int idProdus;
	private int cantitate;
	private double total;

	public ProdusComandat(int idComanda, int idProdus, int cantitate, double total) {
		super();
		this.idComanda = idComanda;
		this.idProdus = idProdus;
		this.cantitate = cantitate;
		this.total = total;
	}

	public int getIdProdus() {
		return idProdus;
	}


	public void setIdProdus(int idProdus) {
		this.idProdus = idProdus;
	}


	public int getCantitate() {
		return cantitate;
	}

	public void setCantitate(int cantitate) {
		this.cantitate = cantitate;
	}

	public double getTotal() {
		return total;
	}

	public void setTotal(double total) {
		this.total = total;
	}

	public int getIdComanda() {
		return idComanda;
	}

	public void setIdComanda(int idComanda) {
		this.idComanda = idComanda;
	}
	
	
}
