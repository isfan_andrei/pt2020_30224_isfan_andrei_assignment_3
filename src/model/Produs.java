package model;

public class Produs {
	private int idProdus;
	private String numeProdus;
	private int cantitate;
	private double pret;
	
	public Produs(int idProdus, String numeProdus, int cantitate, double pret) {
		super();
		this.idProdus = idProdus;
		this.numeProdus = numeProdus;
		this.cantitate = cantitate;
		this.pret = pret;
	}
	public Produs() {
		// TODO Auto-generated constructor stub
	}
	public String getNumeProdus() {
		return numeProdus;
	}
	public void setNumeProdus(String numeProdus) {
		this.numeProdus = numeProdus;
	}
	public int getCantitate() {
		return cantitate;
	}
	public void setCantitate(int cantitate) {
		this.cantitate = cantitate;
	}
	public double getPret() {
		return pret;
	}
	public void setPret(double pret) {
		this.pret = pret;
	}
	public int getIdProdus() {
		return idProdus;
	}
	public void setIdProdus(int idProdus) {
		this.idProdus = idProdus;
	}
	
}
