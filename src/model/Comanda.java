package model;

public class Comanda {
	private int idComanda;
	private int idClient;
	private double total;

	public Comanda(int idComanda, int idClient, double total) {
		super();
		this.idComanda = idComanda;
		this.idClient = idClient;
	}

	public int getIdClient() {
		return idClient;
	}

	public void setIdClient(int idClient) {
		this.idClient = idClient;
	}

	public int getIdComanda() {
		return idComanda;
	}

	public void setIdComanda(int idComanda) {
		this.idComanda = idComanda;
	}
	
}
