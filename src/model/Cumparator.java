package model;

public class Cumparator {
	private int idClient;
	private String numeClient;
	private String adresa;
	
	public Cumparator(int idClient, String numeClient, String adresa) {
		super();
		this.idClient = idClient;
		this.numeClient = numeClient;
		this.adresa = adresa;
	}
	public Cumparator() {
		// TODO Auto-generated constructor stub
	}
	public int getIdClient() {
		return idClient;
	}
	public void setIdClient(int idClient) {
		this.idClient = idClient;
	}
	public String getNumeClient() {
		return numeClient;
	}
	public void setNumeClient(String numeClient) {
		this.numeClient = numeClient;
	}
	public String getAdresa() {
		return adresa;
	}
	public void setAdresa(String adresa) {
		this.adresa = adresa;
	}
	
	
}
